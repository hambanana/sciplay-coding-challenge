﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class People : MonoBehaviour
{
    //A list of people.
    private List<Person> people = new List<Person>();
    List<YearInfo> yearInfo = new List<YearInfo>();

    //A list of years that the most people are alive.
    private List<int> yearsMostPeopleAreAlive = new List<int>();

    //The year to start checking for people that are alive.
    public int startingYear = 1900;
    //The year to stop checking for people that are alive.
    public int endingYear = 2000;
    //The number of people to create.
    public int numberOfPeople = 100000;

    private void Awake()
    {
        //Create a specified number of people
        CreateRandomPeople(numberOfPeople);
        //Get the years that the most people are alive.
        yearsMostPeopleAreAlive = GetYearMostPeopleAreAlive(people);
        //Use a singular or plural string to start the sentence to print to the console.
        string preYearString = "The year the most people are alive is ";
        if (yearsMostPeopleAreAlive.Count > 1) {
            preYearString = "The years the most people are alive are ";
        }

        Debug.Log(preYearString + string.Join(", ", yearsMostPeopleAreAlive));

    }

    /// <summary>
    /// Creates a specified number of people (names, birth years, death years) and adds them to a list.
    /// </summary>
    /// <param name="numberOfPeople">The number of people to create.</param>
    public void CreateRandomPeople(int numberOfPeople)
    {
        //A loop that creates a specified number of people.
        for (int i = 0; i < numberOfPeople; i++)
        {
            //Create a random name from a set list of first and last names.
            string name = Utility.firstNames[Random.Range(0, Utility.firstNames.Count)] + " " + Utility.lastNames[Random.Range(0, Utility.lastNames.Count)];
            //Set a random year for a person to be born.
            int birthYear = Random.Range(startingYear, endingYear + 1);
            //Set a random year for a person to die. Ensure they cannot die before they were born.
            int deathYear = Random.Range(birthYear, endingYear + 1);
            //Create a new person with the randomly generated name, birth year, and death year. Add them to the list of people.
            people.Add(new Person {
                Name = name,
                BirthYear = birthYear,
                DeathYear = deathYear
            });
        }
    }
    /// <summary>
    /// Get the years that the most people are alive.
    /// </summary>
    /// <param name="newPeople">A list of people to check.</param>
    /// <returns>The years with the most people alive</returns>
    public List<int> GetYearMostPeopleAreAlive(List<Person> newPeople) {

        List<int> returnValue = new List<int>();

        int currentAliveCount = 0;
        //Loop through the specified years.
        for (int i = startingYear; i <= endingYear; i++)
        {
            //For each year loop through all of the people.
            foreach (Person person in newPeople)
            {
                //Check to see if a person was alive in the specified year. Includes the year they were born and the year they died.
                if (i >= person.BirthYear && i <= person.DeathYear)
                {
                    currentAliveCount++;
                }
            }
            //Add the information about each year to a list.
            yearInfo.Add(new YearInfo
            {
                Year = i,
                NumberOfPeople = currentAliveCount
            });

            //reset the count of the current number of people alive.
            currentAliveCount = 0;
        }
        //Get the maximum amount of people alive.
        int maxNumberOfPeople = yearInfo.Max(x => x.NumberOfPeople);

        //Retrieve the years that match the maximum number of people alive.
        IEnumerable<int> years = (from x in yearInfo where x.NumberOfPeople == maxNumberOfPeople select x.Year);
        List<int> convertedYears = years.ToList();
        returnValue = convertedYears;

        //return the year with the most people alive.
        return returnValue;
    }
    
}
