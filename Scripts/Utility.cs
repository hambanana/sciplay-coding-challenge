﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utility
{
    //A list of first names.
    public static readonly List<string> firstNames = new List<string>()
    {
        "Antonio",
        "Ashley",
        "Ben",
        "Bob",
        "Barry",
        "Britanny",
        "Carl",
        "Chris",
        "Christina",
        "Charlie",
        "Charolette",
        "Dan",
        "Demi",
        "Danielle",
        "Hari",
        "Judy",
        "Jacob",
        "John",
        "Kristy",
        "Larry",
        "Lawerence"
    };
    //A list of last names.
    public static readonly List<string> lastNames = new List<string>()
    {
        "Smith",
        "Johnson",
        "Williams",
        "Jones",
        "Brown",
        "Davis",
        "Miller",
        "Wilson",
        "Moore",
        "Taylor",
        "Anderson",
        "Thomas",
        "Jackson",
        "White",
        "Harris",
        "Martin",
        "Thompson",
        "Garcia",
        "Robinson",
        "Clark"
    };
}
