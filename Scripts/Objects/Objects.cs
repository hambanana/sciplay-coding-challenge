﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Information about a person.
/// </summary>
public class Person
{
    public string Name { get; set; }
    public int BirthYear { get; set; }
    public int DeathYear { get; set; }
}
/// <summary>
/// Information about a year.
/// </summary>
public class YearInfo
{
    public int Year { get; set; }
    public int NumberOfPeople { get; set; }
}

